//Напишите структуру Employee, которая содержит поля name, salary и position.
//Напишите метод, который повышает зарплату сотрудника на указанную сумму.

package main

import "fmt"

type Employee struct {
	name     string
	salary   int
	position string
}

func (e *Employee) newSalary(s int) {
	e.salary += s
}
func main() {
	emp := Employee{
		name:     "Oleg",
		salary:   2000,
		position: "1",
	}
	emp.newSalary(1000)
	fmt.Printf("Name: %s, Salary: %d, Position: %s", emp.name, emp.salary, emp.position)
}
