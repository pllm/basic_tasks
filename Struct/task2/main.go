//Напишите структуру Circle, которая содержит поле radius.
//Напишите методы для вычисления диаметра, длины окружности и площади круга.

package main

import (
	"fmt"
	"math"
)

type Circle struct {
	radius float64
}

func (c Circle) Diameter() float64 {
	return c.radius * 2
}

func (c Circle) Area() float64 {
	return math.Pi * (c.radius * c.radius)
}
func main() {
	circle := Circle{
		radius: 5,
	}
	fmt.Println(Circle.Diameter(circle))
	fmt.Println(Circle.Area(circle))
}
