//Напишите структуру Student, которая содержит поля name, age и grade.
//Напишите метод, который печатает информацию о студенте в формате "Имя: {name}, Возраст: {age}, Класс: {grade}".

package main

import (
	"fmt"
)

type Student struct {
	name  string
	age   int
	grade string
}

func (s Student) GetValues(student Student) {
	fmt.Printf("Имя: %s, Возраст: %d, Класс: %s", s.name, s.age, s.grade)
}
func main() {
	student := Student{
		name:  "Oleg",
		age:   22,
		grade: "9",
	}
	student.GetValues(student)
}
