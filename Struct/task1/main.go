//Напишите структуру Rectangle, которая содержит поля width и height.
//Напишите методы для вычисления периметра и площади прямоугольника.
package main

import "fmt"

type Rectangle struct {
	width  int
	height int
}

func (r Rectangle) Area() int {
	return r.width * r.height
}

func (r Rectangle) Perimeter() int {
	return 2 * (r.width + r.height)
}
func main() {
	rect := Rectangle{
		width:  4,
		height: 3,
	}
	fmt.Println(Rectangle.Area(rect))
	fmt.Println(Rectangle.Perimeter(rect))
}
