//Напишите структуру BankAccount, которая содержит поля balance и owner.
//Напишите методы для снятия и внесения денег со счета.

package main

import "fmt"

type BankAccount struct {
	user  string
	money int
}

func (b *BankAccount) PutMoney(s int) {
	b.money = b.money - s
}

func (b *BankAccount) GetMoney(s int) {
	b.money = b.money + s
}
func main() {
	acc := BankAccount{
		user:  "Anton",
		money: 32000,
	}
	fmt.Println("Before:", acc)
	acc.PutMoney(1000)
	fmt.Println("Put money:", acc)
	acc.GetMoney(30000)
	fmt.Println("Get money:", acc)
}
