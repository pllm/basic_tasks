package main

import "fmt"

func text(str string) func() {
	return func() {
		fmt.Println(str)
	}
}
func main() {
	//a := text("some text")
	//a()
	text("some text")()
}
