package main

import "fmt"

func main() {
	a := getFunc()
	a("some text")

	b := increment()
	fmt.Println(b())
}

func getFunc() func(string) {
	return func(str string) {
		fmt.Println(str)
	}
}

func increment() func() int {
	i := 0
	return func() int {
		i++
		return i
	}
}
