//Напишите программу, которая создает канал и несколько горутин, которые получают значения из канала.
//Когда все значения будут получены, программа должна закрыть канал.
package main

import (
	"fmt"
	"sync"
)

func main() {
	mess := []int{15, 12, 14, 20}
	ch := make(chan int)
	wg := sync.WaitGroup{}

	wg.Add(2)
	go func() {
		defer wg.Done()
		for _, v := range mess {
			ch <- v
		}
		close(ch)
	}()

	for i := 0; i < 2; i++ {
		go func() {
			defer wg.Done()
			for val := range ch {
				fmt.Println(val)
			}
		}()
	}
	wg.Wait()
}
