//Напишите программу, которая создает 2 канала и передает значения между ними.
//Передача должна осуществляться в обе стороны.

package main

import "fmt"

func main() {
	ch1 := make(chan int)
	ch2 := make(chan int)
	go sendValue(10, ch1)
	go sendChToCh(ch1, ch2)
	fmt.Println(<-ch2)
	go sendValue(15, ch1)
	fmt.Println(<-ch1)
}

func sendValue(val int, c chan int) {
	c <- val
}

func sendChToCh(c1 chan int, c2 chan int) {
	res := <-c1
	c2 <- res
}
