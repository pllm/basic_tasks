///Напишите программу, которая создает канал и несколько горутин, которые отправляют значения в канал.
//Когда все значения будут отправлены, программа должна закрыть канал и вывести все полученные значения.

package main

import (
	"fmt"
	"sync"
)

func main() {
	mess1 := 25
	mess2 := 30
	mess3 := 39
	ch := make(chan int)

	wg := sync.WaitGroup{}
	wg.Add(2)

	go func() {
		defer wg.Done()
		ch <- mess1
		ch <- mess3
	}()

	go func() {
		defer wg.Done()
		ch <- mess2
	}()

	go func() {
		for val := range ch {
			fmt.Println(val)
		}
	}()
	wg.Wait()
	close(ch)
}
