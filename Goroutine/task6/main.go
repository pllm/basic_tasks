//Напишите программу, которая создает канал и горутину, которая отправляет на канал текущее время каждую секунду.
//Другая горутина должна получать значения из канала и выводить их на экран.

package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	ch := make(chan string)
	wg := sync.WaitGroup{}
	wg.Add(2)
	go func() {
		defer wg.Done()
		for range time.Tick(time.Second) {
			ch <- time.Now().Format("2006-01-02 15:04:05")
		}
		close(ch)
	}()

	go func() {
		defer wg.Done()
		for val := range ch {
			fmt.Println(val)
		}

	}()
	wg.Wait()
}
