//Напишите программу, которая создает 2 канала и несколько горутин.
//Первая горутина отправляет значения в первый канал, а вторая горутина получает значения из второго канала.
//Когда все значения будут отправлены и получены, программа должна закрыть оба канала.

package main

import (
	"fmt"
	"sync"
)

func main() {
	ch1 := make(chan int, 6)
	ch2 := make(chan int, 3)
	ch2 <- 25
	ch2 <- 15

	wg := sync.WaitGroup{}

	wg.Add(2)

	go func() {
		defer close(ch1)
		for i := 1; i < 5; i++ {
			ch1 <- i
		}
		fmt.Println(<-ch1)
	}()

	go func() {
		defer close(ch2)
		readCountLimit := 2
		for val := range ch2 {
			ch2 <- val
			readCountLimit--
		}
		fmt.Println(readCountLimit)
	}()
}
