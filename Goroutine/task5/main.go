//Напишите программу, которая создает канал и горутину, которая отправляет на канал случайные числа
//от 1 до 100 каждые 100 миллисекунд. Другая горутина должна получать значения из канала и выводить их на экран.

package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	ch := make(chan int)
	wg := sync.WaitGroup{}
	wg.Add(2)

	go func() {
		defer wg.Done()
		for range time.Tick(time.Second) {
			ch <- rand.Intn(99) + 1
		}
		close(ch)
	}()

	go func() {
		defer wg.Done()
		for val := range ch {
			fmt.Println(val)
		}
	}()
	wg.Wait()
}
