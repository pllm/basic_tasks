//Задача 4
//Напишите функцию, которая находит среднее арифметическое значений в заданном массиве float64 с помощью указателей.
package main

import "fmt"

func main() {
	arr := [6]float64{3.1, 1.6, 6.7, 7.3, 3.5, 8.9}
	fmt.Println(getAverageArr(&arr))
}

func getAverageArr(arr *[6]float64) float64 {
	var sum float64
	for i := 0; i < len(*arr); i++ {
		sum += arr[i]
	}
	aver := sum / float64(len(arr))
	return aver
}
