/// Задача 1
//// Напишите функцию, которая меняет местами значения двух переменных типа int с помощью указателей.

package main

import "fmt"

func main() {
	firstInt := 50
	secondInt := 100
	fmt.Println(firstInt, secondInt)
	fmt.Println(changesTwoVar(firstInt, secondInt))

}

func changesTwoVar(firstInt, secondInt int) (int, int) {
	first := &firstInt
	second := &secondInt
	first, second = &secondInt, &firstInt
	return *first, *second
}
