//Задача 3
//Напишите функцию, которая принимает указатель на int и возвращает указатель на его квадрат.
package main

import "fmt"

func main() {
	num := 4
	// fmt.Println(*getSquareNum(&num))
	fmt.Println(getSquareNum(&num))
}

func getSquareNum(num *int) int {
	square := (*num) * (*num)
	res := &square

	return *res
}
