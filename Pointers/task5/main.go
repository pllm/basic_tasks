//Задача 5
//Напишите функцию, которая принимает указатель на структуру Person и изменяет ее поле Name.

package main

import "fmt"

type Person struct {
	Name string
	Age  int
}

func main() {
	person := Person{
		Name: "Nick",
		Age:  19,
	}
	fmt.Println(changePersonName(&person))
}

func changePersonName(p *Person) Person {
	p.Name = "John"
	return *p
}
