//Задача 8
//Напишите функцию, которая принимает указатель на структуру Point и изменяет ее поля X и Y на заданные значения.
package main

import "fmt"

type Point struct {
	x int
	y int
}

func main() {
	point := Point{
		x: 5,
		y: 7,
	}
	fmt.Println(changeFieldsOfStruct(&point))

}
func changeFieldsOfStruct(*Point) Point {
	p := Point{
		x: 9,
		y: 3,
	}
	return p
}
