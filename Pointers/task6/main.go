//Задача 6
//Напишите функцию, которая принимает два указателя на int и возвращает указатель на их сумму.

package main

import "fmt"

func main() {
	num1 := 10
	num2 := 15
	fmt.Println(sumTwoNum(&num1, &num2))
}

func sumTwoNum(num1, num2 *int) int {
	sum := *num1 + *num2
	res := &sum
	return *res
}
