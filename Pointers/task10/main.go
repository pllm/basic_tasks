//Задача 10
//Напишите функцию, которая принимает указатель на структуру Circle и возвращает указатель на ее площадь.

package main

import (
	"fmt"
	"math"
)

type Circle struct {
	radius float64
}

func main() {
	circle := Circle{
		radius: 4,
	}

	fmt.Println(getArea(&circle))
}

func getArea(c *Circle) float64 {
	res := 2 * math.Pi * c.radius * c.radius
	area := &res
	return *area
}
