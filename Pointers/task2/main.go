//Задача 2
//Напишите функцию, которая находит минимальное и максимальное значения в заданном массиве int с помощью указателей.
package main

import "fmt"

func main() {
	sliceIntNum := []int{1, 2, 34, 4, 41, 9, 12, 7, 19, 78}
	min, max := minMax(sliceIntNum)
	fmt.Println(*min, *max)
}

func minMax(sliceIntNum []int) (*int, *int) {
	min, max := &sliceIntNum[0], &sliceIntNum[0]
	for i, v := range sliceIntNum {
		if v < *min {
			min = &sliceIntNum[i]
		}
		if v > *max {
			max = &sliceIntNum[i]
		}
	}
	return min, max
}
