//Задача 9
//Напишите функцию, которая принимает указатель на массив int и возвращает указатель на его первый элемент.
package main

import "fmt"

func main() {
	arr := [5]int{3, 4, 7, 8, 9}
	fmt.Println(pointOfValue(arr))
}

func pointOfValue(arr [5]int) int {
	p := &arr[0]
	return *p
}
