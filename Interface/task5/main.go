package main

import "fmt"

type Students struct {
	Name   string
	Age    int
	Course []Courses
}

type Courses struct {
	CourseName  string
	Description string
	Mark        int
}

func main() {
	stud := []Students{
		{
			Name: "John",
			Age:  28,
			Course: []Courses{
				{
					CourseName:  "Physics",
					Description: "course about the study of physics",
					Mark:        4,
				},
				{
					CourseName:  "Philosophy",
					Description: "course about the study of philosophy",
					Mark:        5,
				},
				{
					CourseName:  "Statistics",
					Description: "course about the study of statistics",
					Mark:        3,
				},
			},
		},
		{
			Name: "Alex",
			Age:  19,
			Course: []Courses{
				{
					CourseName:  "Physics",
					Description: "course about the study of physics",
					Mark:        4,
				},
				{
					CourseName:  "Philosophy",
					Description: "course about the study of philosophy",
					Mark:        3,
				},
				{
					CourseName:  "Statistics",
					Description: "course about the study of statistics",
					Mark:        4,
				},
			},
		},
		{
			Name: "Mark",
			Age:  21,
			Course: []Courses{
				{
					CourseName:  "Physics",
					Description: "course about the study of physics",
					Mark:        3,
				},
				{
					CourseName:  "Philosophy",
					Description: "course about the study of philosophy",
					Mark:        3,
				},
				{
					CourseName:  "Statistics",
					Description: "course about the study of statistics",
					Mark:        5,
				},
			},
		},
	}
	mapStud := make(map[int]Students, len(stud))
	for k, v := range stud {
		mapStud[k] = v
	}
	GetStudents(mapStud)
}
func GetStudents(m map[int]Students) {
	for k, v := range m {
		fmt.Println(k, v)
	}
}
