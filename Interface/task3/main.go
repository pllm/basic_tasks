// Задача 3
// Напишите интерфейс Shape, который определяет методы для вычисления площади и периметра фигуры.

package main

import "fmt"

type Shape interface {
	Area() float64
	Perimeter() float64
}

type Square struct {
	size int
}

func (s Square) Area() float64 {
	return float64(s.size) * float64(s.size)
}

func (s Square) Perimeter() float64 {
	return float64(s.size) * 2
}
func main() {
	testSquare1 := Square{5}
	testSquare2 := Square{10}
	fmt.Println(testSquare1.Area())
	fmt.Println(testSquare1.Perimeter())

	fmt.Println(testSquare2.Area())
	fmt.Println(testSquare2.Perimeter())
}
