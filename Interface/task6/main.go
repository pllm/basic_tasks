package main

import "fmt"

type User struct {
	Name string
	Age  int
}
type Eligible interface {
	isEligible()
}

func (u User) isEligible(user User) bool {
	res := false
	if u.Age >= 18 {
		res = true
	}

	return res
}

func checkEligible(user User) {
	result := true
	if user.isEligible(user) == result {
		fmt.Println("User is eligible")
	}
	fmt.Println("User is not eligible")
}

func main() {
	user := User{
		Name: "Kate",
		Age:  17,
	}
	user1 := User{
		Name: "Ann",
		Age:  21,
	}
	checkEligible(user)
	checkEligible(user1)

}
