// Задача 1
// Напишите функцию, которая принимает на вход любой тип данных и выводит его тип.
package main

import (
	"fmt"
	"reflect"
)

func main() {
	var name string
	var age int
	var result bool
	fmt.Println(getType(name))
	fmt.Println(getType(age))
	fmt.Println(getType(result))
}

func getType(t interface{}) interface{} {
	return reflect.TypeOf(t)
}
