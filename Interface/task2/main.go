// Задача 2
// Напишите функцию, которая принимает на вход любой тип данных и возвращает true, если это строка, и false в противном случае.
package main

import (
	"fmt"
)

func main() {
	name := "John"
	age := 21
	fmt.Println(getResult(name))
	fmt.Println(getResult(age))
}

func getResult(t interface{}) bool {
	var result bool
	switch t.(type) {
	case string:
		result = true
	default:
		result = false
	}
	return result
}
