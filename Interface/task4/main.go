//Напишите интерфейс Sortable, который определяет метод Len() для получения длины списка,
//метод Swap() для обмена элементами и метод Less() для сравнения двух элементов списка.

//Напишите функцию, которая сортирует список элементов любого типа данных, реализующих интерфейс Sortable.

package main

import (
	"fmt"
	"sort"
)

type Sortable interface {
	Len() int
	Swap(i, j int)
	Less(i, j int) bool
}
type Accounts struct {
	Name    string
	Balance int
}
type Account []Accounts

func (acc Account) Len() int {
	return len(acc)
}

func (acc Account) Swap(i, j int) {
	acc[i], acc[j] = acc[j], acc[i]
}

func (acc Account) Less(i, j int) bool {
	return acc[i].Balance < acc[j].Balance
}

func main() {
	accoun := []Accounts{
		{"Egor", 100000},
		{"Oleg", 45000},
		{"Alex", 500000},
		{"john", 40000},
	}
	fmt.Println(accoun)

	SortElement(accoun)
	fmt.Println(accoun)
}

func SortElement(elems []Accounts) {
	sort.Sort(Account(elems))
}
