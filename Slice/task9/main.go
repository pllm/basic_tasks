// Задача 9
// Напишите функцию, которая возвращает новый слайс, состоящий из элементов исходного слайса в обратном порядке.
package main

import "fmt"

func main() {
	sliceIntNumbers := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	fmt.Println(reverseSlice(sliceIntNumbers))
}

func reverseSlice(sliceIntNumbers []int) []int {
	newReverSlice := make([]int, 0)
	for idx := range sliceIntNumbers {
		newReverSlice = append(newReverSlice, sliceIntNumbers[len(sliceIntNumbers)-1-idx])
	}
	return newReverSlice
}
