//Задача 1
//Напишите функцию, которая находит максимальный элемент в слайсе целых чисел.
package main

import "fmt"

func main() {
	var sliceInt []int
	sliceInt = []int{1, 3, 7, 9, 2, 13, 34, 11, 6, 8}
	fmt.Println(maxValue(sliceInt))
}

func maxValue(sliceInt []int) int {
	maxVal := sliceInt[0]
	for _, val := range sliceInt {
		if val > maxVal {
			maxVal = val
		}
	}

	return maxVal
}
