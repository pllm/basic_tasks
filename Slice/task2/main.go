// Задача 2
// Напишите функцию, которая удаляет дубликаты из слайса строк.
package main

import "fmt"

func main() {
	var sliceStr []string
	sliceStr = []string{"abc", "cas", "assda", "lang", "ter", "hello", "gall", "cas", "ter"}
	fmt.Println(deleteDuplicate(sliceStr))

}

func deleteDuplicate(sliceStr []string) []string {
	mapDupl := make(map[string]bool)
	var newSlice []string
	for _, str := range sliceStr {
		if _, val := mapDupl[str]; !val {
			mapDupl[str] = true
			newSlice = append(newSlice, str)
		}

	}
	return newSlice
}
