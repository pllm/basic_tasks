// Задача 5
// Напишите функцию, которая возвращает новый слайс, состоящий только из четных чисел из исходного слайса.
package main

import "fmt"

func main() {
	sliceIntNumber := []int{4, 5, 34, 54, 61, 11, 77, 87, 10, 44}
	fmt.Println(getNewSlice(sliceIntNumber))
}

func getNewSlice(sliceIntNumber []int) []int {
	newSlice := make([]int, 0)
	for _, num := range sliceIntNumber {
		if num%2 == 0 {
			newSlice = append(newSlice, num)
		}
	}
	return newSlice
}
