// Задача 6
// Напишите функцию, которая проверяет, содержит ли слайс хотя бы один элемент, удовлетворяющий заданному условию.
package main

import "fmt"

func main() {
	sliceIntNumber := []int{1, 9, 5, 7, 8, 3, 44, 7}
	fmt.Println(firstKratNum(sliceIntNumber))
}

func firstKratNum(sliceIntNumber []int) (int, bool) {
	var count bool
	var kratNum int
	for _, num := range sliceIntNumber {
		if num%3 == 0 {
			count = true
			kratNum = num
			break
		}
	}
	return kratNum, count
}
