// Задача 7
// Напишите функцию, которая удаляет из слайса все элементы, не удовлетворяющие заданному условию.
package main

import "fmt"

func main() {
	sliceNumbers := []int{3, 45, 66, 723, 67, 33, 1, 2, 9, 44}
	fmt.Println("before:", sliceNumbers)
	filterNumbers((&sliceNumbers))
	fmt.Println("after:", sliceNumbers)
}

func filterNumbers(sliceNumbers *[]int) []int {
	// const maxVal = 60
	for i := 0; i < len(*sliceNumbers); i++ {
		if (*sliceNumbers)[i] > 60 {
			*sliceNumbers = append((*sliceNumbers)[:i], (*sliceNumbers)[i+1:]...)
			i--
		}

	}
	return *sliceNumbers
}
