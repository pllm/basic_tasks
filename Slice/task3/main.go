// Задача 3
// Напишите функцию, которая сортирует слайс строк в порядке возрастания.
package main

import (
	"fmt"
	"sort"
)

func main() {
	var sliceStr []string
	sliceStr = []string{"a", "ca", "assdad", "lang", "ter", "helloas", "gall", "casassas", "terasasasa"}
	fmt.Println(sortSlice(sliceStr))
}

func sortSlice(sliceStr []string) []string {
	sort.Slice(sliceStr, func(i1, i2 int) bool {
		return len(sliceStr[i1]) < len(sliceStr[i2])
	})

	return sliceStr

}
