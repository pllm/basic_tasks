// Задача 10
// Напишите функцию, которая удаляет из слайса все элементы, равные нулю.
package main

import "fmt"

func main() {
	sliceIntNumbers := []int{1, 4, 6, 0, 7, 0, 10, 3, 0}
	fmt.Println(deleteZeroValue(sliceIntNumbers))
}

func deleteZeroValue(sliceIntNumbers []int) []int {
	for i := 0; i < len(sliceIntNumbers); i++ {
		if sliceIntNumbers[i] == 0 {
			sliceIntNumbers = append(sliceIntNumbers[:i], sliceIntNumbers[i+1:]...)
			i--
		}

	}
	return sliceIntNumbers
}
