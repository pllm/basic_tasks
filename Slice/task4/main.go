// Задача 4
// Напишите функцию, которая объединяет два слайса целых чисел и удаляет дубликаты.

package main

import (
	"fmt"
)

func main() {

	firstSlice := []int{1, 3, 5, 6, 7, 9, 11, 33}
	secondSlice := []int{11, 34, 7, 116, 121, 10, 3}

	fmt.Println(deleteDuplicateAndJoin(firstSlice, secondSlice))
}

func deleteDuplicateAndJoin(firstSlice, secondSlice []int) []int {
	firstSlice = append(firstSlice, secondSlice...)
	var newSlice []int
	mapDupl := make(map[int]bool)
	for _, num := range firstSlice {
		if _, val := mapDupl[num]; !val {
			mapDupl[num] = true
			newSlice = append(newSlice, num)
		}

	}
	return newSlice
}
