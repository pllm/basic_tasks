// Задача 9
// Напишите функцию, которая сортирует map по значениям в порядке возрастания и возвращает новый map.
package main

import (
	"fmt"
	"sort"
)

func main() {
	mapAge := map[string]int{"Ann": 19, "Kate": 21, "John": 40, "Sam": 14}
	fmt.Println(sortMapValue(mapAge))
}

type keyValue struct {
	Key   string
	Value int
}

var sortedStruct []keyValue

func sortMapValue(mapAge map[string]int) []keyValue {
	for key, value := range mapAge {
		sortedStruct = append(sortedStruct, keyValue{key, value})
	}
	sort.Slice(sortedStruct, func(i, j int) bool {
		return sortedStruct[i].Value < sortedStruct[j].Value
	})
	return sortedStruct

}
