// Задача 8
// Напишите функцию, которая удаляет из map все элементы, ключи которых удовлетворяют заданному условию.
package main

import "fmt"

func main() {
	mapIntNum := map[int]int{1: 2, 2: 3, 3: 4, 4: 10, 5: 22, 6: 46, 7: 90}
	fmt.Println(filterMapValue(mapIntNum))
}

func filterMapValue(mapIntValue map[int]int) map[int]int {
	for k, v := range mapIntValue {
		if v < 40 {
			delete(mapIntValue, k)
		}
	}
	return mapIntValue
}
