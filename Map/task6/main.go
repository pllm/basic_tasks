// Задача 6
// Напишите функцию, которая возвращает новый map, в котором ключи и значения исходного map переведены в нижний регистр.
package main

import (
	"fmt"
	"strings"
)

func main() {
	mapString := map[string]string{"KEY1": "VAL1", "KEY2": "VAL2", "KEY3": "VAL3", "KEY4": "VAL4"}
	fmt.Println(changeMapReg(mapString))
}

func changeMapReg(mapString map[string]string) map[string]string {
	modifiedMap := make(map[string]string, len(mapString))
	for k, v := range mapString {
		modifiedMap[strings.ToLower(k)] = strings.ToLower(v)
	}
	return modifiedMap
}
