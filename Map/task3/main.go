// Задача 3
// Напишите функцию, которая возвращает новый map, содержащий только те элементы, ключи которых удовлетворяют заданному условию.
package main

import "fmt"

func main() {
	myMap := map[int]int{1: 10, 2: 20, 3: 30, 4: 40, 5: 50}
	result := getNewMap(myMap)
	fmt.Println(result)
}

func getNewMap(myMap map[int]int) map[int]int {
	newMap := make(map[int]int)
	for k, v := range myMap {
		if v > 40 {
			newMap[k] = v
		}
	}
	return newMap
}
