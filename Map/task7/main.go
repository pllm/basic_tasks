// Задача 7
// Напишите функцию, которая возвращает сумму всех значений в map.
package main

import "fmt"

func main() {
	mapIntNum := map[int]int{1: 2, 2: 3, 3: 4, 4: 10, 5: 22, 6: 46, 7: 90}
	fmt.Println(getSumValues(mapIntNum))
}

func getSumValues(mapIntNum map[int]int) int {
	result := 0
	for _, v := range mapIntNum {
		result += v

	}
	return result
}
