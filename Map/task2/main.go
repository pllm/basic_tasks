// Задача 2
// Напишите функцию, которая объединяет два map[string]int и возвращает новый map,
// содержащий значения из обоих исходных map, если ключи не дублируются.
// Если ключи дублируются, то значение должно быть суммой значений для каждого ключа.

package main

import "fmt"

func main() {
	firstMap := map[string]int{"a": 10, "b": 20, "c": 30, "d": 40}
	secondMap := map[string]int{"c": 50, "a": 10, "d": 60, "w": 100}
	fmt.Println(unionMap(firstMap, secondMap))
}

func unionMap(firstMap, secondMap map[string]int) map[string]int {
	newUnionMap := make(map[string]int, len(firstMap))
	for k, v := range firstMap { //прошлись по первой мапе
		newUnionMap[k] += v // прибавили значени в новую мапу
	}
	for k, v := range secondMap { // прошлись по второй мапе
		newUnionMap[k] += v //добавили значение
	}
	return newUnionMap //возвращаем новую мапу
}
