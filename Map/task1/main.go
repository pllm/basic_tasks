// Задача 1
// Напишите функцию, которая считает количество вхождений каждого элемента в слайс и возвращает результат в виде map[string]int,
// где ключ - это элемент, а значение - количество его вхождений.

package main

import "fmt"

func main() {
	sliceStr := []string{"ini", "olo", "ini", "ini", "ter", "momo", "mono"}
	fmt.Println(countEntryNumbers(sliceStr))
}

func countEntryNumbers(sliceStr []string) map[string]int {
	countEntry := make(map[string]int, len(sliceStr))
	for _, val := range sliceStr {
		countEntry[val] = countEntry[val] + 1

	}
	return countEntry
}
