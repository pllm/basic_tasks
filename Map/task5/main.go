// Задача 5
// Напишите функцию, которая возвращает новый map, в котором ключи и значения исходного map поменяны местами.
package main

import "fmt"

func main() {
	myMap := map[int]string{1: "a", 2: "b", 3: "c", 4: "d", 5: "e", 6: "f"}
	fmt.Println(swapMapValue(myMap))
}

func swapMapValue(myMap map[int]string) map[string]int {
	newMap := make(map[string]int)
	for k, v := range myMap {
		newMap[v] = k
	}
	return newMap
}
