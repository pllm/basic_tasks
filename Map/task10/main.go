// Задача 10
// Напишите функцию, которая возвращает новый map, в котором значения исходного map умножены на заданный множитель.

package main

import "fmt"

func main() {
	mapIntNum := map[int]int{1: 10, 2: 20, 3: 30, 4: 40, 5: 50}
	fmt.Println(multiplMapValue(mapIntNum))
}

func multiplMapValue(mapIntNum map[int]int) map[int]int {
	const fact = 3
	newMap := make(map[int]int, len(mapIntNum))
	for k, v := range mapIntNum {
		newMap[k] = (v * fact)
	}
	return newMap
}
