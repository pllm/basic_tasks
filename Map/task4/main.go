// Задача 4
// Напишите функцию, которая проверяет, содержит ли map значение, удовлетворяющее заданному условию.
package main

import "fmt"

func main() {
	myMap := map[int]int{1: 10, 2: 20, 3: 30, 4: 40, 5: 50}
	a := isMapValue(myMap)
	fmt.Println(a)
}

func isMapValue(myMap map[int]int) bool {
	var assert bool
	for _, v := range myMap {
		if v == 50 {
			assert = true
		}
	}
	return assert
}
