package main

import (
	"errors"
	"fmt"
)

type User struct {
	id   int
	age  int
	name string
}

var userStore = make(map[int]User)

func checkUserId(num int, users map[int]User) (User, error) {
	for _, v := range users {
		if v.id == num {
			return v, nil
		}
	}
	return User{}, errors.New("user not found")
}
func main() {
	userStore = map[int]User{
		1: {id: 1, age: 32, name: "Oleg"},
		2: {id: 2, age: 45, name: "John"},
		3: {id: 3, age: 25, name: "Tom"},
		4: {id: 4, age: 37, name: "Maks"},
	}
	userId := 5
	user, err := checkUserId(userId, userStore)
	if err != nil {
		err := fmt.Errorf("user with id %d not found", userId)
		fmt.Println(err)
	} else {
		fmt.Println(user)
	}

}
